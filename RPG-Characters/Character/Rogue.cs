﻿using RPG_Characters.Equipment;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Character
{
    public class Rogue : Adventurer
    {
        public Rogue(string name)
        {
            // Base and level
            this.name = name;
            this.level = 1;
            this.baseStats = new PrimaryAttribute(2, 6, 1);
            this.levelUpStats = new PrimaryAttribute(1, 4, 1);

            // Equipement
            this.equipment = new Dictionary<ItemSlots, Item> { { ItemSlots.WEAPON, null } };
            this.canEquipWeapon = new WeaponType[] { WeaponType.DAGGERS, WeaponType.SWORDS };
            this.canEquipArmor = new ArmorType[] { ArmorType.LEATHER, ArmorType.MAIL };
            UpdateStats();
            this.mainStatType = "dex";
        }

        
    }
}

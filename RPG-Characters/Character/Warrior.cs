﻿using RPG_Characters.Equipment;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Character
{
    public class Warrior : Adventurer
    {
        public Warrior(string name)
        {
            // Base and level
            this.name = name;
            this.level = 1;
            this.baseStats = new PrimaryAttribute(5, 2, 1);
            this.levelUpStats = new PrimaryAttribute(3, 2, 1);

            // Equipement
            this.equipment = new Dictionary<ItemSlots, Item> { { ItemSlots.WEAPON, null } };
            this.canEquipWeapon = new WeaponType[] { WeaponType.AXES, WeaponType.HAMMERS, WeaponType.SWORDS };
            this.canEquipArmor = new ArmorType[] { ArmorType.MAIL, ArmorType.PLATE };
            UpdateStats();
            this.mainStatType = "str";
        }


    }
}

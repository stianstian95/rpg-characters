﻿using RPG_Characters.Equipment;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Character
{
    public enum ItemSlots
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
    public abstract class Adventurer
    {
        // Attributes
        public string name { get; set; }
        public int level { get; set; }
        public PrimaryAttribute baseStats { get; set; }
        public PrimaryAttribute levelUpStats { get; set; }
        public PrimaryAttribute totalStat { get; set; }
        public int mainStat { get; set; }
        public string mainStatType { get; set; }


        // Equipment
        public Dictionary<ItemSlots, Item> equipment = new Dictionary<ItemSlots, Item>();
        public WeaponType[] canEquipWeapon = { };
        public ArmorType[] canEquipArmor = { };

        // Method that takes the weapon dps and calculate the characters damage
        public double DoDamage()
        {
            double weaponDPS = 1;
            if (equipment[ItemSlots.WEAPON] != null)
            {
                Weapon weapon = (Weapon)equipment[ItemSlots.WEAPON];
                weaponDPS = weapon.Dps();
            }
            return weaponDPS * (1 + (this.mainStat / 100));
        }

        // Method that increases the characters level and stats
        public void LevelUp()
        {
            this.level = level + 1;
            this.baseStats = this.baseStats + this.levelUpStats;
            Console.WriteLine(this.level.ToString());
            UpdateStats();
        }

        // Method that checks the weapontype and level requirement before 
        // adding it to the equipment dictoionary
        public string Equip(Item item)
        {
            if (item.slot == ItemSlots.WEAPON)
            {
                Weapon weapon = (Weapon)item;
                if (this.canEquipWeapon.Contains(weapon.weaponType) && this.level >= weapon.levelReq)
                {
                    this.equipment[ItemSlots.WEAPON] = weapon;
                    UpdateStats();
                    return "New weapon equipped!";
                }
                else
                {
                    throw new InvalidWeaponException("I cant equip that");
                }
            }
            else
            {
                Armor armor = (Armor)item;
                if (this.canEquipArmor.Contains(armor.armorType) && this.level >= armor.levelReq)
                {
                    this.equipment[armor.slot] = armor;
                    UpdateStats();
                    return "New armor equipped!";
                }
                else
                {
                    throw new InvalidArmorException("I cant use this");
                }
            }
        }

        // Method to update the characters stats with armor and level stats added
        public void UpdateStats()
        {
            this.totalStat = new PrimaryAttribute(0, 0, 0);
            this.totalStat += this.baseStats;
            foreach (KeyValuePair<ItemSlots, Item> entry in equipment)
            {
                if (entry.Key != ItemSlots.WEAPON)
                {
                    Armor armor = (Armor)entry.Value;
                    this.totalStat += armor.stats;
                }
            }
            //mainStat = Math.Max(totalStat.strength, Math.Max(totalStat.intelligence, totalStat.dexterity));
            switch (this.mainStatType)
            {
                case "str":
                    this.mainStat = this.totalStat.strength;
                    break;
                case "dex":
                    this.mainStat = this.totalStat.dexterity;
                    break;
                case "int":
                    this.mainStat = this.totalStat.intelligence;
                    break;
                default:
                    this.mainStat = 0;
                    break;
            }
        }

        // Method that prints the characters stats, damage and equipment
        // in the console.
        public string DisplayAdventurer()
        {
            StringBuilder sb = new StringBuilder(50);
            sb.AppendLine("Name: " + this.name);
            sb.AppendLine("Level: " + this.level);
            sb.AppendLine("Strength: " + this.totalStat.strength);
            sb.AppendLine("Dexterity: " + this.totalStat.dexterity);
            sb.AppendLine("Intelligence: " + this.totalStat.intelligence);
            sb.AppendLine("Damage: " + DoDamage());

            foreach (var item in equipment)
            {
                if (item.Value != null)
                {
                    sb.AppendLine(item.Value.DisplayItem());
                }

            }
            return sb.ToString();

        }

        // Custom exception for armor
        public class InvalidArmorException : Exception
        {
            public InvalidArmorException(string errMessage) { }
        }

        // Custom exception for weapons
        public class InvalidWeaponException : Exception
        {
            public InvalidWeaponException(string errMessage) { }
        }

    }
}

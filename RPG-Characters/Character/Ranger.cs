﻿using RPG_Characters.Equipment;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Character
{
    public class Ranger : Adventurer
    {
        public Ranger(string name)
        {
            // Base and level
            this.name = name;
            this.level = 1;
            this.baseStats = new PrimaryAttribute(1, 7, 1);
            this.levelUpStats = new PrimaryAttribute(1, 5, 1);

            // Equipement
            this.equipment = new Dictionary<ItemSlots, Item> { { ItemSlots.WEAPON, null } };
            this.canEquipWeapon = new WeaponType[] { WeaponType.BOWS };
            this.canEquipArmor = new ArmorType[] { ArmorType.LEATHER, ArmorType.MAIL };
            UpdateStats();
            this.mainStatType = "dex";
        }

        
    }
}

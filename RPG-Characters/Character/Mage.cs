﻿using RPG_Characters.Equipment;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Character
{
    public class Mage : Adventurer
    {
        public Mage(string name)
        {
            // Base and level
            this.name = name;
            this.level = 1;
            this.baseStats = new PrimaryAttribute(1, 1, 8);
            this.levelUpStats = new PrimaryAttribute(1, 1, 5);

            // Equipement
            this.equipment = new Dictionary<ItemSlots, Item> { { ItemSlots.WEAPON, null } };
            //{ { ItemSlots.HEAD, null },
            //    { ItemSlots.BODY, null }, { ItemSlots.LEGS, null}, { ItemSlots.WEAPON, null } }; // might not work
            
            this.canEquipWeapon = new WeaponType[] { WeaponType.STAFFS, WeaponType.WANDS };
            this.canEquipArmor = new ArmorType[] { ArmorType.CLOTH };
            UpdateStats();
            this.mainStatType = "int";
        }
    }
}

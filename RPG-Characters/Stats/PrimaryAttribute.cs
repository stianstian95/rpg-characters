﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Stats
{
    public class PrimaryAttribute
    {
        // Variable field
        public int strength { get; set; }
        public int dexterity { get; set; }
        public int intelligence { get; set; }

        // Constructor
        public PrimaryAttribute(int strength, int dexterity, int intelligence)
        {
            this.strength = strength;
            this.dexterity = dexterity;
            this.intelligence = intelligence;
        }

        // Overloading the '+' operator
        public static PrimaryAttribute operator +(PrimaryAttribute lhs, PrimaryAttribute rhs)
        {
            return new PrimaryAttribute(
                lhs.strength += rhs.strength,
                 lhs.dexterity += rhs.dexterity,
                  lhs.intelligence += rhs.intelligence

            );
        }
    }
}

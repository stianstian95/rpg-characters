﻿using RPG_Characters.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Equipment
{
    public abstract class Item
    {
        // Variable field
        public string name { get; set; }
        public int levelReq { get; set; }
        public ItemSlots slot { get; set; }

        // constructor
        public Item(string name, int levelReq, ItemSlots slot)
        {
            this.name = name;
            this.levelReq = levelReq;
            this.slot = slot;
        }

        // For reaching the variables in the child class
        public abstract string SubDisplay();
        // Method to display items
        public string DisplayItem()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("NAME: " + this.name);
            sb.AppendLine("level requirement: " + levelReq);
            sb.AppendLine("\tLevel: " + this.slot);
            sb.AppendLine(SubDisplay());
            return sb.ToString();
        }
    }
}

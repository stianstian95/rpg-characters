﻿using RPG_Characters.Character;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Equipment
{
    // enum of different armor types
    public enum ArmorType
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
    public class Armor : Item
    {
        // Variable fields
        public ArmorType armorType { get; set; }
        public PrimaryAttribute stats { get; set; }

        // Constructor
        public Armor(string name, int levelReq, ItemSlots slot, ArmorType type, PrimaryAttribute stats) : base(name, levelReq, slot)
        {
            this.armorType = type;
            this.stats = stats;
        }

        // Method to display armor, used in the Item class
        public override string SubDisplay()
        {
            StringBuilder sb = new StringBuilder(50);
            sb.AppendLine("\tArmorType: " + armorType);
            sb.AppendLine("\tStrength: " + stats.strength);
            sb.AppendLine("\tDexterity: " + stats.dexterity);
            sb.AppendLine("\tIntelligence: " + stats.intelligence);
            return sb.ToString();

        }
    }
}

﻿using RPG_Characters.Character;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Equipment
{
    // Enum to hold the different weapon types that are allowed
    public enum WeaponType
    {
        AXES,
        BOWS,
        DAGGERS,
        HAMMERS,
        STAFFS,
        SWORDS,
        WANDS,
    }

    public class Weapon : Item
    {
        // Variable fields
        public WeaponType weaponType { get; set; }
        public int damage;
        public double attackSpeed;

        // Constructor
        public Weapon(string name, int level, ItemSlots slot, WeaponType type, int damage, double attackSpeed) : base(name, level, slot)
        {
            this.weaponType = type;
            this.damage = damage;
            this.attackSpeed = attackSpeed;
        }

        // Method that returns the weapons damage times attackspeed(DPS)
        public double Dps()
        {
            return damage * attackSpeed;
        }

        // Method to display weapon, used in the Item class
        public override string SubDisplay()
        {
            StringBuilder sb = new(50);
            sb.AppendLine("\tWeaponType: " + weaponType);
            sb.AppendLine("\tbaseDmg: " + damage);
            sb.AppendLine("\tatkSpeed: " + attackSpeed);
            return sb.ToString();
        }

    }

}

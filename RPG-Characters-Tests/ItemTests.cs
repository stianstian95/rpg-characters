﻿using RPG_Characters.Character;
using RPG_Characters.Equipment;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static RPG_Characters.Character.Adventurer;

namespace RPG_Characters_Tests
{
    public class ItemTests
    {
        [Fact] //1) If a character tries to equip a high level weapon, InvalidWeaponException should be thrown.
        public void EquipWeapon_InValidLevel_ThrowException()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            Weapon weapon = new Weapon("Common axe", 2, ItemSlots.WEAPON, WeaponType.AXES, 7, (float)1.1);
            // Act
            // Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(weapon));
        }

        [Fact] //2) If a character tries to equip a high level armor piece, InvalidArmorException should be thrown.
        public void EquipArmor_InValidLevel_ThrowException()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            PrimaryAttribute stats = new PrimaryAttribute(1, 0, 0);
            Armor armor = new Armor("Common plate body armor", 2, ItemSlots.BODY, ArmorType.PLATE, stats);
            // Act
            // Assert
            Assert.Throws<InvalidArmorException>(() => warrior.Equip(armor));
        }

        [Fact] //3) If a character tries to equip the wrong weapon type, InvalidWeaponException should be thrown.
        public void EquipWeapon_InvalidType_ThrowException()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            Weapon weapon = new Weapon("Common bow", 1, ItemSlots.WEAPON, WeaponType.BOWS, 12, (float)0.8);
            // Act 
            // Assert
            Assert.Throws < InvalidWeaponException>(() => warrior.Equip(weapon));
        }

        [Fact] //4) If a character tries to equip the wrong armor type, InvalidArmorException should be thrown.
        public void EquipArmor_InvalidType_ThrowException()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            PrimaryAttribute stats = new PrimaryAttribute(0, 0, 5);
            Armor armor = new Armor("common cloth head armor", 1, ItemSlots.HEAD, ArmorType.CLOTH, stats);
            // Act
            // Assert
            Assert.Throws<InvalidArmorException>(() => warrior.Equip(armor));
        }

        [Fact] //5) If a character equips a valid weapon, a success message should be returned
        public void EquipWeapon_Success_ReturnSuccessMssg()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            Weapon weapon = new Weapon("COMMON AXE", 1, ItemSlots.WEAPON, WeaponType.AXES, 7, 1.1);
            // Act
            string expected = "New weapon equipped!";
            string actual = warrior.Equip(weapon);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact] //6) If a character equips a valid armor piece, a success message should be returned
        public void EquipArmor_Success_ReturnSuccessMssg()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            PrimaryAttribute stats = new PrimaryAttribute(1, 0, 0);
            Armor armor = new Armor("Common plate body armor", 1, ItemSlots.BODY, ArmorType.PLATE, stats);
            // Act
            string expected = "New armor equipped!";
            string actual = warrior.Equip(armor);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact] //7) Calculate Damage if no weapon is equipped.
        public void DoDamage_WithoutWeapon_ReturnCorrectDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            // Act
            double expected = 1 * (1 + (5 / 100));
            double actual = warrior.DoDamage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact] //8) Calculate Damage with valid weapon equipped.
        public void DoDamage_WithWeapon_ReturnCorrectDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            Weapon weapon = new Weapon("COMMON AXE", 1, ItemSlots.WEAPON, WeaponType.AXES, 7, 1.1);
            // Act
            double expected = (7 * 1.1) * (1 + (5 / 100));
            warrior.Equip(weapon);
            double actual = warrior.DoDamage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact] //9) Calculate Damage with valid weapon and armor equipped.
        public void DoDamage_WithWeaponAndArmor_ReturnCorrectDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            Weapon weapon = new Weapon("COMMON AXE", 1, ItemSlots.WEAPON, WeaponType.AXES, 7, 1.1);
            PrimaryAttribute stats = new PrimaryAttribute(1, 0, 0);
            Armor armor = new Armor("Common plate body armor", 1, ItemSlots.BODY, ArmorType.PLATE, stats);
            // Act
            warrior.Equip(armor);
            warrior.Equip(weapon);
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
            double actual = warrior.DoDamage();
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}

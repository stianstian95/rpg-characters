using RPG_Characters.Character;
using RPG_Characters.Stats;
using System;
using Xunit;

namespace RPG_Characters_Tests
{
    public class CharacterTests
    {  
        [Fact] //1) A character is level 1 when created.
        public void Initialize_Warrior_BeLevel1()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            int expected = 1;
            // Act
            int actual = warrior.level;
            // Assert
            Assert.Equal(expected, actual);
        }


        [Fact] //2) When a character gains a level, it should be level 2.
        public void LevelUp_WarrLevel1_BeLevel2()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            warrior.LevelUp();
            int expected = 2;
            // Act
            int actual = warrior.level;
            // Assert
            Assert.Equal(expected, actual);
        }


        [Fact] //3) Each character class is created with the proper default attributes.
        public void Initialize_Warrior_CorrectStats()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(5, 2, 1);
            PrimaryAttribute actual = warrior.baseStats;
            // Assert
            Assert.Equal(expected.strength, actual.strength);
            Assert.Equal(expected.dexterity, actual.dexterity);
            Assert.Equal(expected.intelligence, actual.intelligence);
        }

        [Fact] //3) Each character class is created with the proper default attributes.
        public void Initialize_Rogue_CorrectStats()
        {
            // Arrange
            Rogue rogue= new Rogue("Stian");
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(2, 6, 1);
            PrimaryAttribute actual = rogue.baseStats;
            // Assert
            Assert.Equal(expected.strength, actual.strength);
            Assert.Equal(expected.dexterity, actual.dexterity);
            Assert.Equal(expected.intelligence, actual.intelligence);
        }

        [Fact] //3) Each character class is created with the proper default attributes.
        public void Initialize_Ranger_CorrectStats()
        {
            // Arrange
            Ranger ranger = new Ranger("Stian");
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(1, 7, 1);
            PrimaryAttribute actual = ranger.baseStats;
            // Assert
            Assert.Equal(expected.strength, actual.strength);
            Assert.Equal(expected.dexterity, actual.dexterity);
            Assert.Equal(expected.intelligence, actual.intelligence);
        }

        [Fact] //3) Each character class is created with the proper default attributes.
        public void Initialize_Mage_CorrectStats()
        {
            // Arrange
            Mage mage = new Mage("Stian");
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(1, 1, 8);
            PrimaryAttribute actual = mage.baseStats;
            // Assert
            Assert.Equal(expected.strength, actual.strength);
            Assert.Equal(expected.dexterity, actual.dexterity);
            Assert.Equal(expected.intelligence, actual.intelligence);
        }


        [Fact] //4) Each character class has their attributes increased when leveling up.
        public void LevelUp_Warrior_CorrectStats()
        {
            // Arrange
            Warrior warrior = new Warrior("Stian");
            warrior.LevelUp();
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(8, 4, 2);
            PrimaryAttribute actual = warrior.totalStat;
            // Assert
            Assert.Equal(expected.strength, actual.strength);
            Assert.Equal(expected.dexterity, actual.dexterity);
            Assert.Equal(expected.intelligence, actual.intelligence);
        }

        [Fact] //4) Each character class has their attributes increased when leveling up.
        public void LevelUp_Rogue_CorrectStats()
        {
            // Arrange
            Rogue rogue = new Rogue("Stian");
            rogue.LevelUp();
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(3, 10, 2);
            PrimaryAttribute actual = rogue.totalStat;
            // Assert
            Assert.Equal(expected.strength, actual.strength);
            Assert.Equal(expected.dexterity, actual.dexterity);
            Assert.Equal(expected.intelligence, actual.intelligence);
        }

        [Fact] //4) Each character class has their attributes increased when leveling up.
        public void LevelUp_Ranger_CorrectStats()
        {
            // Arrange
            Ranger ranger = new Ranger("Stian");
            ranger.LevelUp();
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(2, 12, 2);
            PrimaryAttribute actual = ranger.totalStat;
            // Assert
            Assert.Equal(expected.strength, actual.strength);
            Assert.Equal(expected.dexterity, actual.dexterity);
            Assert.Equal(expected.intelligence, actual.intelligence);
        }

        [Fact] //4) Each character class has their attributes increased when leveling up.
        public void LevelUp_Mage_CorrectStats()
        {
            // Arrange
            Mage mage = new Mage("Stian");
            mage.LevelUp();
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(2, 2, 13);
            PrimaryAttribute actual = mage.totalStat;
            // Assert
            Assert.Equal(expected.strength, actual.strength);
            Assert.Equal(expected.dexterity, actual.dexterity);
            Assert.Equal(expected.intelligence, actual.intelligence);
        }
    }
}

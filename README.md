# RPG Character #
Simple RPG character system
by Stian Selle

## About ##
The game has 4 classes(Mage, ranger, rogue and warrior) 
that all have 3 attributes(strength, dexterity and intelligence).
The characters can level up and equip armor and weapons.
The characters can do damage based on their primary attribute, 
level and the equipment they wear.

## Installation ##
Clone repository and open with Visual Studio.